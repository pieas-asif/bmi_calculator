import 'package:flutter/material.dart';
import 'package:bmi_calculator/constant.dart';

class IconGen extends StatelessWidget {
  IconGen({@required this.iconData, @required this.iconText});

  final IconData iconData;
  final String iconText;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          iconData,
          size: 80.0,
          color: kTextColor,
        ),
        SizedBox(
          height: 15.0,
        ),
        Text(
          iconText,
          style: kLabelTextStyle,
        ),
      ],
    );
  }
}
