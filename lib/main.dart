import 'package:flutter/material.dart';
import 'input_page.dart';

void main() => runApp(BMICalculator());

class BMICalculator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(0xFF0A0D22),
        canvasColor: Color(0xFF0A0D22),
        accentColor: Color(0xFFEB1555),
      ),
      home: InputPage(),
    );
  }
}
