import 'package:flutter/material.dart';

const double kBottomButtonHeight = 80.0;
const Color kActiveCardColor = Color(0xFF1D1F33);
const Color kInactiveCardColor = Color(0xFF111428);
const Color kBottomButtonColor = Color(0xFFEB1555);
const Color kTextColor = Color(0xFFFFFFFF);
const double kTextSize = 18.0;

const Color kSliderThumbColor = Color(0xFFEB1555);
const Color kSliderOverlayColor = Color(0x29EB1555);
const Color kSliderInactiveColor = Color(0xFF8D8E98);
const Color kBMIResultLabelColor = Color(0xFF24D876);

const Color kIconButtonFillColor = Color(0xFF4C4F5E);

const kLabelTextStyle = TextStyle(
  fontSize: kTextSize,
  color: kTextColor,
);

const kNumberTextStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.w900,
  color: kTextColor,
);

const kBMITitleStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.bold,
  color: kTextColor,
);

const kBMIResultLabel = TextStyle(
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
  color: kBMIResultLabelColor,
);

const kBMINumberLabel = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold,
  color: kTextColor,
);
